# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import invoice


def register():
    Pool.register(
        module='account_invoice_report_per_party', type_='model')
    Pool.register(
        invoice.PrintInvoice,
        module='account_invoice_report_per_party', type_='wizard')
    Pool.register(
        invoice.Invoice,
        module='account_invoice_report_per_party', type_='report')
