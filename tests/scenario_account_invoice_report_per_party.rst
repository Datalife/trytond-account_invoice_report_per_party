=========================================
Account Invoice Report Per Party Scenario
=========================================

Imports::

    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_chart, \
    ...     get_accounts
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences

Install account_invoice_report_per_party::

    >>> config = activate_modules('account_invoice_report_per_party')


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> receivable = accounts['receivable']
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']


Create parties::

    >>> Party = Model.get('party.party')
    >>> party = Party(name='Party')
    >>> party.save()
    >>> party2 = Party(name='Party 2')
    >>> party2.save()


Create empty invoices::

    >>> Invoice = Model.get('account.invoice')
    >>> invoice = Invoice()
    >>> invoice.party = party
    >>> invoice.save()
    >>> invoice2 = Invoice()
    >>> invoice2.party = party2
    >>> invoice2.save()
    >>> invoice3 = Invoice()
    >>> invoice3.party = party2
    >>> invoice3.save()

Print invoices::

    >>> print_invoices = Wizard('account.print.invoice', [invoice, invoice2, invoice3])
    >>> len(print_invoices.actions)
    2