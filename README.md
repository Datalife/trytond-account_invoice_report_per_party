datalife_account_invoice_report_per_party
=========================================

The account_invoice_report_per_party module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-account_invoice_report_per_party/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-account_invoice_report_per_party)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
